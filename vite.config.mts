// Plugins
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import Fonts from 'unplugin-fonts/vite'
import Layouts from 'vite-plugin-vue-layouts'
import Vue from '@vitejs/plugin-vue'
import VueRouter from 'unplugin-vue-router/vite'
import Vuetify, {transformAssetUrls} from 'vite-plugin-vuetify'
import {VitePWA} from 'vite-plugin-pwa'

// Utilities
import {defineConfig} from 'vite'
import {fileURLToPath, URL} from 'node:url'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        VueRouter(),
        Layouts(),
        Vue({
            template: {transformAssetUrls},
        }),
        // https://github.com/vuetifyjs/vuetify-loader/tree/master/packages/vite-plugin#readme
        Vuetify({
            autoImport: true,
            styles: {
                configFile: 'src/styles/settings.scss',
            },
        }),
        Components(),
        Fonts({
            google: {
                families: [{
                    name: 'Roboto',
                    styles: 'wght@100;300;400;500;700;900',
                }],
            },
        }),
        AutoImport({
            imports: [
                'vue',
                'vue-router',
            ],
            dts: true,
            eslintrc: {
                enabled: true,
            },
            vueTemplate: true,
        }),
        VitePWA({
            registerType: 'autoUpdate',
            devOptions: {
                enabled: true
            },
            includeAssets: ['ricebowl.ico'],
            manifest: {
                name: "MANGA KON APP",
                short_name: "MANGA KON",
                description: "Your manga reading application",
                start_url: "/",
                display: "standalone",
                background_color: "#FFFFFF",
                theme_color: "#009688",
                icons: [
                    {
                        src: "/android-chrome-96x96.png",
                        sizes: "96x96",
                        type: "image/png",
                    },
                    {
                        src: "/favicon-16x16.png",
                        sizes: "16x16",
                        type: "image/png",
                    },
                    {
                        src: "/favicon-32x32.png",
                        sizes: "32x32",
                        type: "image/png",
                    },
                    {
                        src: "/mstile-150x150.png",
                        sizes: "270x270",
                        type: "image/png",
                    },
                ],
                screenshots: [
                    {

                        src: "source/screenshot_wide.png",
                        sizes: "1920x1080",
                        type: "image/png",
                        form_factor: "wide",
                        label: "Web App"
                    },
                    {

                        src: "source/screenshot_mobile_wide.png",
                        sizes: "1920x1080",
                        type: "image/png",
                        form_factor: "wide",
                        label: "Mobile App"
                    }
                ]
            }

        }),
    ],
    define: {'process.env': {}},
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url)),
        },
        extensions: [
            '.js',
            '.json',
            '.jsx',
            '.mjs',
            '.ts',
            '.tsx',
            '.vue',
        ],
    },
    server: {
        port: 3000,
    },
})
