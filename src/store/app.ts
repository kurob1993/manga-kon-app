// Utilities
import {defineStore} from 'pinia'
import {BookmarkData} from "@/interfaces/bookmark-data";

export const useAppStore = defineStore('app', {
  state: () => ({
    title: '',
    source: '',
    sourceTitle: '',
    layout: 'default',
    bookTitle: '',
    bookChapter: '',
    bookSource: '',
    bookmarkList: [] as BookmarkData[],
    chapterSource: ''
  }),

  actions: {
    setBook(bookTitle: string, bookChapter: string, bookSource: string) {
      this.bookTitle = bookTitle
      this.bookChapter = bookChapter
      this.bookSource = bookSource
    },
    setLayout(route: string) {
      if(route === '/book') {
        this.layout = 'reader'
      } else {
        this.layout = 'default'
      }
    },
    setTitle(title: string) {
      this.title = title
    },
    setSource(source: string, title: string) {
      this.source = source
      this.sourceTitle = title
    },
    checkBookmark(link: string) {
      return this.bookmarkList.findIndex((bm) => bm.link == link );
    },
    addBookmark(bookmarkData: BookmarkData) {
      const checkData = this.bookmarkList
        .filter((bookmark) =>
          bookmark.link == bookmarkData.link
        )
      if (checkData.length == 0) {
        this.bookmarkList.push(bookmarkData)
      }
    },
    removeBookmark(link: string) {
      this.bookmarkList = this.bookmarkList
        .filter((b) =>
            b.link != link
        )
    },
    setChapter(chapterSource: string) {
      this.chapterSource = chapterSource
    }
  },
  getters: {
    getButtonBack(): boolean {
      return (this.title !== 'Home') && (this.title !== 'Source') && (this.title !== 'Bookmark')
      // return (this.title in ['Home', 'Source', 'Bookmark']);
    }
  },
  persist: true,
})
