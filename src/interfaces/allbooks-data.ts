interface list {
  title: string;
  img: string;
  link: string;
  lastChapter: string;
  rating: string;
}

export interface AllBooksData {
  list: Array<list>;
}

interface listOpen {
  chapter: string;
  date: string;
  link: string;
}

export interface ListOpenContent {
  desc: string;
  img: string;
  altname: string;
  rating: string;
}

export interface AllBooksOpenData {
  content: ListOpenContent;
  list: Array<listOpen>;
}

export interface bookData {
  list: Array<any>;
}
