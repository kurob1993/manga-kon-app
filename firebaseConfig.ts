// Import the functions you need from the SDKs you need
import { initializeApp } from "@firebase/app";
import { getAnalytics } from "@firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDqXDmXc2O6C071RjMhYbYuExXaGGtarNk",
  authDomain: "spbu-palima.firebaseapp.com",
  projectId: "spbu-palima",
  storageBucket: "spbu-palima.appspot.com",
  messagingSenderId: "834814290849",
  appId: "1:834814290849:web:08cd16594da54add4ef9e0",
  measurementId: "G-2TV994N9PF"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
