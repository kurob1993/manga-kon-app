/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Plugins
import { registerPlugins } from '@/plugins'

// Components
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

// Composables
import { createApp } from 'vue'

const app = createApp(App).use(VueAxios, axios)
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
axios.defaults.baseURL = 'https://manga-kon-cli.vercel.app'

registerPlugins(app)

app.mount('#app')
