import apiClient from '../api/CoreService'

export default class Source {
  list(): Promise<any> {
    return apiClient.get(`/source/list`)
  }

  allbooks(source: string): Promise<any> {
    return apiClient.get(`/source/all-books?source=${source}`)
  }

  chapter(source: string, bookSource: string): Promise<any> {
    return apiClient.get(`/source/chapter?source=${source}&url=${bookSource}`)
  }

  book(source: string, bookSource: string): Promise<any> {
    return apiClient.get(`/source/book?source=${source}&url=${bookSource}`)
  }

  mangalist(source: string, order: string): Promise<any> {
    return apiClient.get(`/source/mangalist?source=${source}&order=${order}`)
  }
}

