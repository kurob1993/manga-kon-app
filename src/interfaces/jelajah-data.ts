interface list {
  id: string;
  title: string;
  img: string;
  url: string;
}

export interface JelajahData {
  list: Array<list>;
}
