interface list {
  title: string;
  img: string;
  link: string;
}

export interface PerpustakaanData {
  list: Array<list>;
}
