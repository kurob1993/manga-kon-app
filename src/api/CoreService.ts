import axios, { AxiosInstance } from 'axios'

const apiClient: AxiosInstance = axios.create({ headers: {} })

export default apiClient
