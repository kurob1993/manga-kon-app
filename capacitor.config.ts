import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.spbupalima.app',
  appName: 'spbu-palima',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;
