/**
 * router/index.ts
 *
 * Automatic routes for `./src/pages/*.vue`
 */

// Composables
import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router/auto'
import { setupLayouts } from 'virtual:generated-layouts'
import { useAppStore } from '@/store/app'

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  extendRoutes: setupLayouts,
})

router.beforeEach((to, from) => {
  const store = useAppStore()
  store.setLayout(to.path)

  return true
})

export default router
