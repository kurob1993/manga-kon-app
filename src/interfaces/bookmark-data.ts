export interface BookmarkData {
  source: string;
  title: string;
  img: string;
  link: string;
}
